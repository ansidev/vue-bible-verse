BibleVerse Examples:

Bible Verse with default style
```js
<VueBibleVerse title="John 3:16" version="193" addr="JHN.3.16" />
```
Bible Verse with custom color
```js
<VueBibleVerse title="John 3:16" version="193" addr="JHN.3.16" background-color="#ff5900" />
<VueBibleVerse title="John 3:16" version="193" addr="JHN.3.16" text-color="#ff5900" background-color="#eee" />
```

Bible Verse with border radius
```js
<VueBibleVerse title="John 3:16" version="193" addr="JHN.3.16" background-color="#ff5900" border-radius="5px" />
```
