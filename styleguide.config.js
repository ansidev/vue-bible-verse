
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
  title: 'Vue Bible Verse',
  sections: [
    {
      name: 'Bible',
      components: 'src/components/Bible/*.vue'
    },
  ],
  styleguideDir: 'docs',
  webpackConfig: {
    module: {
      rules: [
        {
          test: /\.vue$/,
          exclude: /node_modules/,
          loader: 'vue-loader'
        },
        {
          test: /\.(styl|stylus)$/,
          loader: 'style-loader!css-loader!stylus-loader'
        }
      ]
    },
    plugins: [
      new VueLoaderPlugin()
    ]
  }
}
