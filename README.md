# VueBibleVerse Component

Vue Component to display Bible Verse from bible.com

## install
```
npm install vue-bible-verse
```
or

```
yarn add vue-bible-verse
```
## Usage

Bible Verse with default style
```js
<VueBibleVerse title="John 3:16" version="193" addr="JHN.3.16" />
```
Bible Verse with custom style
```js
<VueBibleVerse title="John 3:16" version="193" addr="JHN.3.16" background-color="#006df0" />
```
```js
<VueBibleVerse title="John 3:16" version="193" addr="JHN.3.16" text-color="#006df0" background-color="#eee" />
```
